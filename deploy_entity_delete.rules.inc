<?php

/**
 * @file
 * Rules implementations.
 */

module_load_include('inc', 'deploy', 'deploy.rules');

/**
 * Implements hook_rules_action_info().
 */
function deploy_entity_delete_rules_action_info()
{
  return array(
      'deploy_entity_delete_rules_action_add_to_managed_plan' => array(
          'label' => t('Add entity to managed deployment plan to be removed from the target sites.'),
          'group' => t('Deploy'),
          'parameter' => array(
              'plan_name' => array(
                  'type' => 'text',
                  'label' => t('Plan name'),
                  'description' => t('The plan to add the entity to.'),
                  'options list' => 'deploy_rules_manager_plan_get_options'
              ),
              'entity' => array(
                  'type' => 'entity',
                  'label' => t('Entity'),
                  'description' => t('The entity that shall be added to the plan.'),
                  'wrapped' => TRUE,
              ),
          ),
      )
  );
}


/**
 * Action callback for the "Add to deploy plan" action.
 */
function deploy_entity_delete_rules_action_add_to_managed_plan($plan_name, $entity_wrapper)
{
  if (!deploy_manager_plan_load($plan_name)) {
    watchdog('deploy', 'Cannot add to non-managed plan %name', array('%name' => $plan_name), WATCHDOG_ERROR);
    return;
  }

  $entity = $entity_wrapper->value();
  $entity_type = $entity_wrapper->type();

  $deploy_entity_delete_wrapper = new stdClass();
  $deploy_entity_delete_wrapper->referenced_type = $entity_type;
  $deploy_entity_delete_wrapper->referenced_id = $entity_wrapper->getIdentifier();
  $deploy_entity_delete_wrapper->referenced_uuid = $entity->uuid;
  entity_save('deploy_entity_delete', $deploy_entity_delete_wrapper);

  deploy_manager_add_to_plan($plan_name, 'deploy_entity_delete', $deploy_entity_delete_wrapper);
}
